# Kelompok KA-14

[![pipeline status](https://gitlab.com/kelompok-ka14/tugas-kelompok-2/badges/master/pipeline.svg)](https://gitlab.com/kelompok-ka14/tugas-kelompok-2/commits/master)
[![coverage report](https://gitlab.com/kelompok-ka14/tugas-kelompok-2/badges/master/coverage.svg)](https://gitlab.com/kelompok-ka14/tugas-kelompok-2/commits/master)

* [Deskripsi](#deskripsi)
* [Daftar Fitur](#daftar-fitur)
* [Link HerokuApp](http://ka4teen2.herokuapp.com/)

## Nama anggota
- Ardiaf Rizqy Aula (Vendor List)
- Dipta Laksmana Baswara D. (Vendor Review)
- Dennis R. Lesmono (Vendor Food)
- Ferenica Dwi Putri (Vendor Info)

## Deskripsi
Aplikasi yang kita buat adalah platform web yang digunakan untuk menghubungkan antara orang yang hendak menyelenggarakan resepsi pernikahan dan ingin mencari sebuah catering makanan untuk pernikahannya dan para pemiliki catering yang menawarkan penawaran menarik bagi para pasangan yang hendak menyelenggarakan resepsi pernikahan. 

Manfaat dari web ini ialah para pasangan dapat dengan mudah mencari catering makanan dengan harga yang pas serta setidaknya memiliki gambaran terhadap catering makanan tersebut, dan juga web ini dapat membantu menaikkan pendapatan para penyedia catering.

## Daftar Fitur
- User dapat mencari penyedia catering
- Penyedia catering dapat menampilkan masakan apa saja yang tersedia, serta harga dari catering tersebut
- User dapat melihat detail vendor dan harganya
- User dapat memberi rating vendor
- User dapat melihat list vendor yang ada
 
    #### Rencana Fitur Tambahan
    - Filter Pencarian
    - Booking Vendor
    - Memberi review

## Pembagian Tugas

1. Dipta Laksmana B.D
	- Membuat model vendor, vendor_review, dan vendor_food
	- Menyiapkan gitlab pipeline dan heroku
	- Menginisiasi project
	- Membuat landing page (home)

2. Ardiaf Rizky Aula
	- Membuat beberapa fungsi di model vendor
	- Membuat page vendor
	- Membuat app vendor-list dan semua yg ada di dalamnya
	- Membuat form untuk menambah vendor ke database
	- Membuat web-service pada ../vendor-list/json
	- Membuat fitur vendor-counter pada vendor page dengan ajax
	- Mengintegrasikan login-system dengan app vendor-list

3. Dennis R. Lesmono
	- Membuat page food di page slug vendor
	- Membuat form untuk menambah food di vendor
	- Membuat app vendor-food dan semua yg ada di dalamnya

4. Ferenica Dwi P.
	- Membuat page vendor/slug
	- Membuat form dan model message
	- Membuat app vendor-info



