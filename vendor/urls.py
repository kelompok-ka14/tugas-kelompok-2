from django.urls import path, include
from vendor import views_add_message

urlpatterns = [
    path('review/', include('vendor_review.urls_add')),
    path('food/', include('vendor_food.urls')),
    path('<str:slug>/', views_add_message.vendor_info),
    path('vendor_info/<str:slug>/', views_add_message.vendor_info_json, name='vendor-info-json'),
]