from django.test import TestCase
from django.test.client import Client
from vendor.models import Vendor, Vendor_Message
from vendor.message_form import MessageForm
from django.core.files.uploadedfile import SimpleUploadedFile
from vendor.views_add_message import vendor_info

class VendorInfo_template_Test(TestCase):
    
    def test_valid_template(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

        vendor_creation = Vendor(
            name="Test Test",
            info="asdasdasdasdasdasdsad",
            price_level=2,
            food_type="Javanese",
            website="http:// something",
            email="asdasd@asdasd.com",
            phone="0123123123",
            vendor_picture=uploaded,
        )
        vendor_creation.save()

        response = self.client.get('/vendor/test-test/')
        self.assertTemplateUsed(response,"vendor/vendor_info.html")
        vendor_creation.delete()

class VendorInfo_url_Test(TestCase):

    def test_vendor_info_url_exist(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

        vendor_creation = Vendor(
            name="Test Test",
            info="asdasdasdasdasdasdsad",
            price_level=2,
            food_type="Javanese",
            website="http:// something",
            email="asdasd@asdasd.com",
            phone="0123123123",
            vendor_picture=uploaded,
        )
        vendor_creation.save()

        response = self.client.get("/vendor/test-test/")
        self.assertEqual(response.status_code,200)
        vendor_creation.delete()