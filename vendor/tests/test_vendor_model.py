import os

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from vendor.models import Vendor, Vendor_Review, Vendor_Message
# contoh buat test case vendor_review model
from vendor.models import Vendor, Vendor_Review

class VendorModelTest(TestCase):
    def test_add_vendor(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif',small_gif,content_type='image/gif')

        vendor_creation  = Vendor(
            name="Test Test",
            info="asdasdasdasdasdasdsad",
            price_level=2,
            food_type="Javanese",
            website="http:// something",
            email="asdasd@asdasd.com",
            phone="0123123123",
            vendor_picture=uploaded,
        )
        vendor_creation.save()

        vendor_creation_1 = Vendor(
            name="Test Test",
            info="asdasdasdasdasdasdsad",
            price_level=2,
            food_type="Javanese",
            website="http:// something",
            email="asdasd@asdasd.com",
            phone="0123123123",
            vendor_picture=uploaded,
        )

        vendor_creation_1.save()

        get_vendor = Vendor.objects.all()
        first_vendor = get_vendor[0]
        second_vendor = get_vendor[1]
        self.assertEqual(len(get_vendor), 2)
        self.assertEqual(first_vendor.name, "Test Test")
        self.assertEqual(first_vendor.info, "asdasdasdasdasdasdsad")
        self.assertEqual(first_vendor.price_level, 2)
        self.assertEqual(first_vendor.food_type, "Javanese")
        self.assertEqual(first_vendor.website, "http:// something")
        self.assertEqual(first_vendor.slug, "test-test")
        self.assertEqual(first_vendor.email, "asdasd@asdasd.com")
        self.assertEqual(first_vendor.phone, "0123123123")
        self.assertEqual(first_vendor.vendor_picture, 'small.gif')

        self.assertEqual(second_vendor.slug, "test-test-1")
        picture_path1 = vendor_creation.vendor_picture.path
        picture_path2 = vendor_creation_1.vendor_picture.path

        vendor_creation.delete()
        vendor_creation_1.delete()
        self.assertFalse(os.path.isfile(picture_path1))
        self.assertFalse(os.path.isfile(picture_path2))


    def test_get_review(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

        vendor_creation = Vendor(
            name="Test Test",
            info="asdasdasdasdasdasdsad",
            price_level=2,
            food_type="Javanese",
            website="http:// something",
            email="asdasd@asdasd.com",
            phone="0123123123",
            vendor_picture=uploaded,
        )

        vendor_creation.save()

        review = Vendor_Review(store_id=1,
                               name="Halo",
                               star_ammount=4,
                               comment="asdasdasd")
        review.save()

        review2 = Vendor_Review(store_id=1,
                                name="Halo",
                                star_ammount=3,
                                comment="asdasdasd")
        review2.save()

        review3 = Vendor_Review(store_id=1,
                                name="Halo",
                                star_ammount=3,
                                comment="asdasdasd")
        review3.save()

        self.assertEqual(vendor_creation.get_star_ammount(), 3.3)
        vendor_creation.delete()

    def test_zero_review(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

        vendor_creation = Vendor(
            name="Test Test",
            info="asdasdasdasdasdasdsad",
            price_level=2,
            food_type="Javanese",
            website="http:// something",
            email="asdasd@asdasd.com",
            phone="0123123123",
            vendor_picture=uploaded,
        )

        vendor_creation.save()
        self.assertEqual(vendor_creation.get_star_ammount(), 0)
        vendor_creation.delete()
