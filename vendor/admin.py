from django.contrib import admin
from . import models
# Register your models here.
admin.site.register(models.Vendor)
admin.site.register(models.Vendor_Food)
admin.site.register(models.Vendor_Review)