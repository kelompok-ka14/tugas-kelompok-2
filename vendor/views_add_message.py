from django.shortcuts import render, redirect
from vendor.models import Vendor_Message, Vendor
from vendor.message_form import MessageForm

from django.core.serializers import serialize
from django.http import HttpResponse, JsonResponse
from utilities import get_dict
from django.middleware.csrf import get_token
import json

# Create your views here.
def vendor_info(request, slug):
    info = Vendor.objects.get(slug = slug)
    price_lvl = "$"*info.price_level
    
    if (request.method == "POST"):
        forms = MessageForm(request.POST)
        if(forms.is_valid()):
            instance = forms.save(commit=False)
            instance.store_id = info.id
            instance.save()
            return redirect('/vendor/'+slug+'/')
        else:
            return render(request, 'vendor/vendor_info.html', {'forms':forms, 'info':info, 'slug':slug, 'price_level':price_lvl})

    return render(request, 'vendor/vendor_info.html', {'forms':MessageForm(), 'info':info, 'slug':slug, 'price_level':price_lvl})

def vendor_info_json(request, slug):
    info = Vendor.objects.get(slug = slug)
    price_lvl = "$"*info.price_level
    
    if (request.method == "POST"):
        forms = MessageForm(data=json.loads(request.body))
        if(forms.is_valid()):
            instance = forms.save(commit=False)
            instance.store_id = info.id
            instance.save()
            return JsonResponse(data={'success':'success'})
        else:
            return JsonResponse(data={'failed':'failed'})




    # try:
    #     request.COOKIES['csrftoken'] = get_token(request)
    #     vendor = Vendor.objects.get(slug=slug)
    #     price_lvl = "$"*info.price_level

    #     if request.method == "POST":
    #         forms = MessageForm(request.POST) if request.content_type != "application/json" else MessageForm(data=json.loads(request.body))
    #         if forms.is_valid():
    #             instance = forms.save(commit=False)
    #             instance.store_id = vendor.id
    #             instance.save()
    #             if(request.content_type == "application/json"):
    #                 data = [i.get_data_dict() for i in Vendor_Message.objects.filter(store_id=vendor.id)[::-1]]
    #                 return JsonResponse(data=get_dict(data=data))
    #             return redirect('/vendor/' + slug + "/")
    #         else:
    #             if(request.content_type == "application/json"):
    #                 errors = {}
    #                 for i in forms:
    #                     for error in i.errors:
    #                         errors[i.name] = errors.get(i.name,[]) + [error]
    #                 return JsonResponse(data=get_dict(message=errors,success=False))
    #             info = Vendor_Message.objects.filter(store_id=vendor.id)[::-1]
    #             return render(request, 'vendor/vendor_info.html', {'forms':forms, 'info':info, 'slug':slug, 'price_level':price_lvl})
    #     info = Vendor_Message.objects.filter(store_id=vendor.id)[::-1]
    #     return render(request, 'vendor/vendor_info.html', {'forms':MessageForm(), 'info':info, 'slug':slug, 'price_level':price_lvl})
    # except Vendor.DoesNotExist:
    #     return HttpResponse("Vendor Does Not Exist", status=404)




            
