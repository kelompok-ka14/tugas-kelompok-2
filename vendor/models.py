import datetime
import os

from django.db.models import *

# Create your models here.
from django.utils.text import slugify


class Vendor(Model):
    name = CharField(max_length=100)
    info = TextField()
    price_level = IntegerField(choices=[(i, i) for i in range(1, 5)], blank=False)
    food_type = CharField(max_length=100)
    website = URLField(max_length=100)
    slug = SlugField()
    email = EmailField()
    phone = CharField(max_length=100)
    vendor_picture = ImageField(blank=False)

    def delete(self, *args, **kwargs):
        if os.path.isfile(self.vendor_picture.path):
            os.remove(self.vendor_picture.path)
        super(Vendor, self).delete(*args, **kwargs)

    def get_star_ammount(self):
        all_review = Vendor_Review.objects.filter(store_id=self.id)
        sum_stars = 0
        for i in all_review:
            sum_stars += i.star_ammount
        if(len(all_review) == 0):
            return 0
        return round(sum_stars/len(all_review),1)

    def get_dollars(self):
        return "$"*self.price_level

    def _get_unique_slug(self):  # For generating slug (url parameter for vendor_review)
        slug = slugify(self.name)
        unique_slug = slug

        num = 1
        while Vendor.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_unique_slug()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

class Vendor_Food(Model):
    store_id = IntegerField()
    name = CharField(max_length=100)
    food_image = ImageField(blank=False)
    store_name = CharField(max_length=100)

    def __str__(self):
        return self.name
    
    def delete(self, *args, **kwargs):
        if os.path.isfile(self.food_image.path):
            os.remove(self.food_image.path)
        super(Vendor_Food, self).delete(*args, **kwargs)

class Vendor_Review(Model):
    store_id = IntegerField()
    name = CharField(max_length=100)
    star_ammount = IntegerField(choices=[(i, i) for i in range(1, 6)], blank=False, default=1)
    comment = TextField(max_length=200)
    date = DateField(auto_now_add=True)

    def __str__(self):
        return self.name

    def get_data_dict(self):
        return {
            "store_id":self.store_id,
            "name":self.name,
            "star_ammount":self.star_ammount,
            "comment":self.comment,
            "date":self.date.strftime("%b. %d, %Y")
        }
class Vendor_Message(Model):
    store_id = IntegerField()
    name = CharField(max_length=100)
    message = TextField()
    date = DateField(auto_now_add=True)

    def __str__(self):
        return self.name
