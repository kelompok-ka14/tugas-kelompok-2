import django.forms as forms
from django.utils.translation import gettext_lazy as _

from vendor.models import Vendor_Message

class MessageForm(forms.ModelForm):
    class Meta:
        model = Vendor_Message
        fields = ['name', 'message']
        labels = {
                'name':_("Name"),
                'message' :_("Message")
                }
        widgets = {
                  'name': forms.TextInput(attrs={
                          'class':'form-control',    
                  }),
                  'message': forms.Textarea(attrs={
                      'style':'max-height: 200px;',
                      'class':'form-control'
                  })
              }