import json

from django.middleware.csrf import get_token
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse

from utilities import get_dict
from vendor.models import Vendor_Review, Vendor

# Create your views here.
from vendor_review.review_form import ReviewForm


def add_review(request, slug):
    try:
        request.COOKIES['csrftoken'] = get_token(request)
        vendor = Vendor.objects.get(slug=slug)
        if request.method == "POST":
            forms = ReviewForm(request.POST) if request.content_type != "application/json" else ReviewForm(data=json.loads(request.body))
            if forms.is_valid():
                instance = forms.save(commit=False)
                instance.store_id = vendor.id
                instance.save()
                if(request.content_type == "application/json"):
                    data = [i.get_data_dict() for i in Vendor_Review.objects.filter(store_id=vendor.id)[::-1]]
                    data = {
                        'data':data,
                        'star':vendor.get_star_ammount()
                    }
                    return JsonResponse(data=get_dict(data=data))
                return redirect('/vendor/review/' + slug + "/")
            else:
                if(request.content_type == "application/json"):
                    errors = {}
                    for i in forms:
                        for error in i.errors:
                            errors[i.name] = errors.get(i.name,[]) + [error]
                    return JsonResponse(data=get_dict(message=errors,success=False))
                reviews = Vendor_Review.objects.filter(store_id=vendor.id)[::-1]
                return render(request, 'vendor_review/review.html',
                              {'forms': forms, 'reviews': reviews, 'info': vendor, 'slug': slug})
        reviews = Vendor_Review.objects.filter(store_id=vendor.id)[::-1]
        return render(request, 'vendor_review/review.html',
                      {'forms': ReviewForm(), 'info': vendor, 'reviews': reviews, 'slug': slug})
    except Vendor.DoesNotExist:
        return HttpResponse("Vendor Does Not Exist", status=404)

def get_review_ajax(request,slug):
    try:
        request.COOKIES['csrftoken'] = get_token(request)
        vendor = Vendor.objects.get(slug=slug)
        reviews = Vendor_Review.objects.filter(store_id=vendor.id)[::-1]
        return JsonResponse(data=get_dict(data=[i.get_data_dict() for i in reviews]))
    except Vendor.DoesNotExist:
        return HttpResponse("Vendor Does Not Exist",status=404)
