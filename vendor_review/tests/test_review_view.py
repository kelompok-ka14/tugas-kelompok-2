import json
from time import sleep

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from vendor.models import Vendor_Review, Vendor


class ReviewViewTest(TestCase):
    def setUp(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

        self.vendor_creation = Vendor(
            name="Test Test",
            info="asdasdasdasdasdasdsad",
            price_level=2,
            food_type="Javanese",
            website="http:// something",
            email="asdasd@asdasd.com",
            phone="0123123123",
            vendor_picture=uploaded,
        )
        self.vendor_creation.save()

    def tearDown(self):
        self.vendor_creation.delete()

    def test_using_correct_template(self):
        response = self.client.get('/vendor/review/test-test/')
        self.assertTemplateUsed(response, 'vendor_review/review.html')

    def test_post_a_review(self):
        request = {
            "name": "Something",
            "star_ammount": 4,
            "comment": "asdasdasdasdasd"
        }
        response = self.client.post("/vendor/review/test-test/", data=request)
        objects = Vendor_Review.objects.filter(store_id=1)
        self.assertEqual(response.status_code, 302)  # Redirect
        self.assertEqual(len(objects), 1)

    def test_post_wrong_review(self):
        request = {
            "store_id": 1,
            "name": "Something",
            "star_ammount": 4
        }
        response = self.client.post("/vendor/review/test-test/", data=request)
        objects = Vendor_Review.objects.filter(store_id=1)
        self.assertEqual(response.status_code,200) #If not redirect django rendered the form again
        self.assertEqual(len(objects),0)

    def test_post_a_review_json(self):
        request = {
            "name": "Something",
            "star_ammount": 4,
            "comment": "asdasdasdasdasd"
        }
        response = self.client.post("/vendor/review/test-test/", data=json.dumps(request),content_type="application/json")
        objects = Vendor_Review.objects.filter(store_id=1)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(objects), 1)
        data = json.loads(response.content)
        self.assertTrue(data['success'])
        self.assertNotEqual(0,len(data['data']))

    def test_post_wrong_review_json(self):
        request = {
            "store_id": 1,
            "name": "Something",
            "star_ammount": 4
        }
        response = self.client.post("/vendor/review/test-test/",data=json.dumps(request),content_type="application/json")
        objects = Vendor_Review.objects.filter(store_id=1)
        self.assertEqual(response.status_code,200) #If not redirect django rendered the form again
        self.assertEqual(len(objects),0)
        data = json.loads(response.content)
        self.assertFalse(data['success'])
        self.assertNotEqual(0,len(data['message']))

    def test_get_review_json(self):
        request = {
            "name": "Something",
            "star_ammount": 4,
            "comment": "asdasdasdasdasd"
        }
        response = self.client.post("/vendor/review/test-test/", data=json.dumps(request),content_type="application/json")
        response = self.client.get("/vendor/review/test-test/json")
        data = json.loads(response.content)
        self.assertTrue(data['success'])
        self.assertEqual(1,len(data['data']))