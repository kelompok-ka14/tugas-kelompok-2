from django.apps import AppConfig


class VendorReviewConfig(AppConfig):
    name = 'vendor_review'
