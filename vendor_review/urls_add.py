from django.urls import path
from vendor_review import views_add_review

urlpatterns = [
    path('<str:slug>/', views_add_review.add_review),
    path('<str:slug>/json',views_add_review.get_review_ajax)
]