from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.core import serializers
from django.http import HttpResponse, JsonResponse
import json

from django.middleware.csrf import get_token

from utilities import *
from django.shortcuts import render

# Create your views here.
from login_system.forms import CreateUserForm


def login_view(request):
    request.COOKIES['csrftoken'] = get_token(request)
    if request.user.is_authenticated:
        return JsonResponse(data=get_dict(success=False, message="Already Logged In"))
    if request.method == "POST":
        data = json.loads(request.body)
        form = AuthenticationForm(request=request, data=data)
        if form.is_valid():
            user = form.get_user()
            login(request=request, user=user)
            return JsonResponse(data=get_dict(data={'name': user.first_name,'is_staff':request.user.is_staff}))
        return JsonResponse(data=get_dict(message=list(form.get_invalid_login_error()), success=False))
    return HttpResponse()


def logout_view(request):
    request.COOKIES['csrftoken'] = get_token(request)
    if not request.user.is_authenticated:
        return JsonResponse(data=get_dict(success=False, message="Not Logged In"))
    if request.method == "POST":
        logout(request)
        return JsonResponse(data=get_dict())
    return HttpResponse()


def signup_view(request):
    request.COOKIES['csrftoken'] = get_token(request)
    if request.user.is_authenticated:
        return JsonResponse(data=get_dict(success=False, message="Already Logged In"))
    if request.method == "POST":
        data = json.loads(request.body)
        form = CreateUserForm(data=data)
        if form.is_valid():
            user = form.save()
            login(request, user=user)
            return JsonResponse(get_dict(data={"name": user.first_name,'is_staff':request.user.is_staff}))
        error_response = {}
        for field in form:
            for error in field.errors:
                error_response[field.name] = error_response.get(field.name, []) + [error]
        return JsonResponse(get_dict(message=error_response, success=False))
    return HttpResponse()


def get_user(request):
    request.COOKIES['csrftoken'] = get_token(request)
    if request.user.is_authenticated:
        user = request.user
        data = {
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'username': user.username,
            'is_staff':user.is_staff,
        }
        return JsonResponse(data=get_dict(data=data))
    else:
        return JsonResponse(data=get_dict(success=False, message="Not Logged In"))
