from django.urls import path

from login_system.views import login_view, logout_view, signup_view, get_user

urlpatterns = [
    path('login/', login_view),
    path('logout/', logout_view),
    path('signup/', signup_view),
    path('data/',get_user)
]