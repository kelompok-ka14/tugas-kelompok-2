from django.test import TestCase


class UrlAccountTest(TestCase):
    def test_login_url(self):
        response = self.client.get('/account/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url(self):
        response = self.client.get('/account/logout/')
        self.assertEqual(response.status_code, 200)

    def test_signup_url(self):
        response = self.client.get('/account/signup/')
        self.assertEqual(response.status_code, 200)
