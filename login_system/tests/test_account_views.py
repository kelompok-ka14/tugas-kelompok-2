import json
from django.contrib.auth.models import User
from django.test import TestCase


class ViewAccountTest(TestCase):
    def setUp(self):
        test = User(username="halohalo",first_name="Sesuatu")
        test.set_password("12341234")
        test.save()

    def test_signup_user(self):
        data = {
            "username": "test",
            "email": "asdasdasd@asdasdasdas.asdasd",
            "password1": "boom123123",
            "password2": "boom123123",
            "first_name": "Sesuatu",
            "last_name": "Something"
        }
        response = self.client.post('/account/signup/', data=json.dumps(data), content_type="application/json")
        self.assertEqual(User.objects.count(), 2)
        self.assertIn('_auth_user_id', self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 200)
        data_json = json.loads(response.content)
        self.assertTrue(data_json['success'])
        self.assertEqual(data_json['data']['name'], "Sesuatu")
        self.assertFalse(data_json['data']['is_staff'])

    def test_signup_fail(self):
        data = {
            "username": "test",
            "email": "asdasdasd@asdasdasdas.asdasd",
            "password1": "asdasdasd123123",
            "password2": "asdasdasd123123",
            "first_name": "Sesuatu",
            "last_name": "Something"
        }
        response = self.client.post('/account/signup/', data=json.dumps(data), content_type="application/json")
        self.assertEqual(User.objects.count(), 1)
        self.assertNotIn('_auth_user_id', self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 200)
        data_json = json.loads(response.content)
        self.assertFalse(data_json['success'])
        self.assertNotIn('name', data_json['data'])
        self.assertNotEqual(len(data_json['message']), 0)

    def test_signin_user(self):
        data = {
            'username': 'halohalo',
            'password': '12341234'
        }
        response = self.client.post('/account/login/', data=json.dumps(data), content_type="application/json")
        self.assertIn('_auth_user_id', self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 200)
        data_json = json.loads(response.content)
        self.assertTrue(data_json['success'])
        self.assertEqual(data_json['data']['name'], "Sesuatu")
        self.assertFalse(data_json['data']['is_staff'])

    def test_signin_fail(self):
        data = {
            'username': 'tempasd',
            'password': '12341234'
        }
        response = self.client.post('/account/login/', data=json.dumps(data), content_type="application/json")
        self.assertNotIn('_auth_user_id', self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 200)
        data_json = json.loads(response.content)
        self.assertFalse(data_json['success'])
        self.assertNotIn('name', data_json['data'])
        self.assertNotEqual(len(data_json['message']), 0)

    def test_logout(self):
        data = {
            'username': 'halohalo',
            'password': '12341234'
        }
        response = self.client.post('/account/login/', data=json.dumps(data), content_type="application/json")

        response = self.client.post('/account/logout/')

        self.assertNotIn('_auth_user_id', self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 200)
        data_json = json.loads(response.content)
        self.assertTrue(data_json['success'])

    def test_signup_after_signin(self):
        data = {
            'username': 'halohalo',
            'password': '12341234'
        }
        response = self.client.post('/account/login/', data=json.dumps(data), content_type="application/json")

        data = {
            "username": "test",
            "email": "asdasdasd@asdasdasdas.asdasd",
            "password1": "boom123123",
            "password2": "boom123123",
            "first_name": "Sesuatu",
            "last_name": "Something"
        }
        response = self.client.post('/account/signup/', data=json.dumps(data), content_type="application/json")


        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), 1)
        data_json = json.loads(response.content)
        self.assertFalse(data_json['success'])

    def test_signin_after_signin(self):
        data = {
            'username': 'halohalo',
            'password': '12341234'
        }
        response = self.client.post('/account/login/', data=json.dumps(data), content_type="application/json")

        data = {
            'username': 'halohalo',
            'password': '12341234'
        }
        response = self.client.post('/account/login/', data=json.dumps(data), content_type="application/json")

        self.assertEqual(response.status_code, 200)
        data_json = json.loads(response.content)
        self.assertFalse(data_json['success'])

    def test_logout_before_signin(self):
        response = self.client.post('/account/logout/')

        self.assertEqual(response.status_code, 200)
        data_json = json.loads(response.content)
        self.assertFalse(data_json['success'])

    def test_get_logged_in_user_detail(self):
        data = {
            'username': 'halohalo',
            'password': '12341234'
        }
        response = self.client.post('/account/login/', data=json.dumps(data), content_type="application/json")

        response = self.client.get('/account/data/')

        self.assertEqual(response.status_code, 200)
        data_json = json.loads(response.content)
        self.assertTrue(data_json['success'])

    def test_get_logged_in_user_detail_before_login(self):
        response = self.client.get('/account/data/')

        self.assertEqual(response.status_code, 200)
        data_json = json.loads(response.content)
        self.assertFalse(data_json['success'])
