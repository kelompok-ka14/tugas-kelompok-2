from django.middleware.csrf import get_token
from django.shortcuts import render
from django.http.response import HttpResponse

# Create your views here.
def index(request):
    request.COOKIES['csrftoken'] = get_token(request)
    return render(request,'homepage.html')