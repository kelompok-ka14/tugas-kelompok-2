from django.test import TestCase
from django.test.client import Client
from vendor.models import Vendor
from vendor_list.form import Vendor_form
from django.core.files.uploadedfile import SimpleUploadedFile

class VendorList_Template_Test(TestCase):
    
    def test_valid_template(self):
        response = Client().get('/vendor_list/')
        self.assertTemplateUsed(response,"vendor_list/vendor_page.html")