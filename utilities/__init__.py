import json


def get_dict(data={}, message="Success", success=True, **kwargs):
    dict_data = {
        'data': data,
        'message': message,
        'success': success
    }
    for key, values in kwargs.items():
        dict_data[key] = values
    return dict_data
