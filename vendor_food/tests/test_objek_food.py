import os

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from vendor.models import Vendor, Vendor_Review, Vendor_Message, Vendor_Food


# contoh buat test case vendor_review model
class VendorModelTest(TestCase):
    def test_add_vendor(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        small_gif1 = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        uploaded1 = SimpleUploadedFile('small1.gif', small_gif1, content_type='image/gif')

        vendor_creation = Vendor(
            name="Test Test",
            info="asdasdasdasdasdasdsad",
            price_level=2,
            food_type="Javanese",
            website="http:// something",
            email="asdasd@asdasd.com",
            phone="0123123123",
            vendor_picture=uploaded,
        )
        vendor_creation.save()

        food_creation = Vendor_Food(
            store_id=vendor_creation.id,
            name="Test Test",
            food_image=uploaded1,
            store_name=vendor_creation.name,
            
        )

        food_creation.save()

        get_vendor = Vendor.objects.all()
        get_food = Vendor_Food.objects.all()
        first_vendor = get_vendor[0]
        self.assertEqual(len(get_vendor), 1)
        self.assertEqual(first_vendor.name, "Test Test")
        self.assertEqual(first_vendor.info, "asdasdasdasdasdasdsad")
        self.assertEqual(first_vendor.price_level, 2)
        self.assertEqual(first_vendor.food_type, "Javanese")
        self.assertEqual(first_vendor.website, "http:// something")
        self.assertEqual(first_vendor.slug, "test-test")
        self.assertEqual(first_vendor.email, "asdasd@asdasd.com")
        self.assertEqual(first_vendor.phone, "0123123123")
        self.assertEqual(first_vendor.vendor_picture, 'small.gif')

        tester_food = get_food[0]
        self.assertEqual(len(get_food), 1)
        self.assertEqual(tester_food.store_id, vendor_creation.id)
        self.assertEqual(tester_food.name, "Test Test")
        self.assertEqual(tester_food.food_image, 'small1.gif')
        self.assertEqual(tester_food.store_name, "Test Test")

        
        picture_path1 = vendor_creation.vendor_picture.path
        picture_path2 = food_creation.food_image.path
        vendor_creation.delete()
        food_creation.delete()
        self.assertFalse(os.path.isfile(picture_path1))
        self.assertFalse(os.path.isfile(picture_path2))
