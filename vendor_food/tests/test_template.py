from django.test import TestCase
from django.test.client import Client
from vendor.models import Vendor, Vendor_Food
from vendor_list.form import Vendor_form
from django.core.files.uploadedfile import SimpleUploadedFile

class VendorList_Template_Test(TestCase):
    def setUp(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        self.uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

        self.vendor_creation = Vendor(
            name="Test Test",
            info="asdasdasdasdasdasdsad",
            price_level=2,
            food_type="Javanese",
            website="http:// something",
            email="asdasd@asdasd.com",
            phone="0123123123",
            vendor_picture=self.uploaded,
        )
        self.vendor_creation.save()

    def tearDown(self):
        self.vendor_creation.delete()
    
    def test_valid_template(self):
        response = Client().get('/vendor/food/test-test/')
        self.assertTemplateUsed(response,"vendor_food/foods.html")