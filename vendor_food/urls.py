from django.urls import path
from vendor_food import views

app_name = "vendor-food"

urlpatterns = [
    path('food_lists/<str:slug>/', views.vendor_food_list_json, name="vendor-food-list-json"),
    path('<str:slug>/', views.addFood),
    
]