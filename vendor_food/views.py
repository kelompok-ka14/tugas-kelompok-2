from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from vendor.models import Vendor_Food, Vendor
from django.core.serializers import serialize
from vendor_food.forms import addFoods

# Create your views here.
def addFood(request, slug):
    vendor = Vendor.objects.get(slug=slug)
    if (request.method == "POST"):
        forms = addFoods(request.POST, request.FILES)
        
        if(forms.is_valid()):
            print(forms)
            instance = forms.save(commit=False)
            instance.store_id = vendor.id 
            instance.store_name = vendor.name
            instance.save()
            return redirect('/vendor/food/'+slug+"/")
        else:
            foods = Vendor_Food.objects.filter(store_id=vendor.id)
            return render(request, 'vendor_food/foods.html', {'form': forms, 'info': vendor, 'foods':foods})
    foods = Vendor_Food.objects.filter(store_id=vendor.id)
    return render(request, 'vendor_food/foods.html', {'form': addFoods(), 'info': vendor, 'slug':slug, 'foods':foods})

def vendor_food_list_json(request, slug):
    vendor = Vendor.objects.get(slug=slug)
    vendor_food = Vendor_Food.objects.filter(store_id=vendor.id)
    vendor_food_list = serialize('json',vendor_food)
    return HttpResponse(vendor_food_list,content_type="text/json-comment-filtered")