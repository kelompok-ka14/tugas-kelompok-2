from django.apps import AppConfig


class VendorFoodConfig(AppConfig):
    name = 'vendor_food'
